package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class PostEvaluacionCancelacion implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        Integer nivelAsesor = (Integer) delegateExecution
                .getVariable("nivelAsesor");

        var resultadoBMN = (List<HashMap>) delegateExecution.getVariable("resultadoCancelacion");

        var resultado = (HashMap<String, Object>) resultadoBMN.get(0);

        boolean cancelacionAprobada = resultado.get("resultadoCancelacion").toString().contains("Aprobado");

        delegateExecution.setVariable("cancelacionAprobada", cancelacionAprobada);

        log.info("""
                                
                P8: Para la póliza %s y con nivel asesor %s, la evaluación de cancelación ha sido aprobada = %s
                """.formatted(policyNumber, nivelAsesor, cancelacionAprobada));


    }
}
