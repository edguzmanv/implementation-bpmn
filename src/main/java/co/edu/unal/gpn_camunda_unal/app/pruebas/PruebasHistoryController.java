package co.edu.unal.gpn_camunda_unal.app.pruebas;

import co.edu.unal.gpn_camunda_unal.app.pruebas.dto.ProcessInstanceDto;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cancellations-history")
@RequiredArgsConstructor
public class PruebasHistoryController {

    private final HistoryService historyService;

    private final RuntimeService runtimeService;

    // no finalizadas
    @GetMapping("/instances")
    public List<ProcessInstanceDto> listProcessInstances() {
        List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery()
                .processDefinitionKey("cancellationProcess")
                .list();

        return processInstances.stream()
                .map(instance -> new ProcessInstanceDto(instance.getId(), instance.getProcessDefinitionId(), instance.isEnded()))
                .collect(Collectors.toList());
    }

    // todas (finalizadas y no finalizadas
    @GetMapping("/historic-instances")
    public List<ProcessInstanceDto> listHistoricProcessInstances() {
        List<HistoricProcessInstance> historicProcessInstances = historyService.createHistoricProcessInstanceQuery()
                .processDefinitionKey("cancellationProcess")
                .list();

        return historicProcessInstances.stream()
                .map(instance -> new ProcessInstanceDto(instance.getId(), instance.getProcessDefinitionId(), instance.getEndTime() != null))
                .collect(Collectors.toList());
    }
}
