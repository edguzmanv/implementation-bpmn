package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class GestionAutorizacionFirmasService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        log.info("""
                                
                P12: Se han gestionado las autorizaciones y firmas 
                para la cancelacion de la póliza %s """
                .formatted(policyNumber));

    }
}
