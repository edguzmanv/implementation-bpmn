package co.edu.unal.gpn_camunda_unal.app.reactivacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class NotificaReactivacionValidacionNoAprobadaService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        log.info("""
                                
                P6: Notifica vía email que la póliza %s no se puede  
                reactivar luego de realizar validación de reactivación"""
                .formatted(policyNumber));

    }
}
