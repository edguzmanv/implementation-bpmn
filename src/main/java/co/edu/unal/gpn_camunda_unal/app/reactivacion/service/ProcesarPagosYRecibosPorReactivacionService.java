package co.edu.unal.gpn_camunda_unal.app.reactivacion.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProcesarPagosYRecibosPorReactivacionService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        var customerNumber = (String) delegateExecution.getVariable("customerNumber");

        log.info("""
                                
                P9: Se procesan los pagos y recibos para la reactivación 
                confirmada de la póliza %s del cliente %s
                """.formatted(policyNumber, customerNumber));

    }
}
