package co.edu.unal.gpn_camunda_unal.app.reactivacion.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class PostEvaluacionReactivacion implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        Integer nivelAsesor = (Integer) delegateExecution
                .getVariable("nivelAsesor");

        var resultadoBMN = (List<HashMap>) delegateExecution.getVariable("resultadoReactivacion");

        var resultado = (HashMap<String, Object>) resultadoBMN.get(0);

        boolean reactivacionArobada = resultado.get("resultadoReactivacion").toString().contains("Aprobado");

        delegateExecution.setVariable("reactivacionAprobada", reactivacionArobada);

        log.info("""
                                
                P5: Para la póliza %s y con nivel asesor %s, la evaluación de reactivación ha sido aprobada = %s
                """.formatted(policyNumber, nivelAsesor, reactivacionArobada));


    }
}
