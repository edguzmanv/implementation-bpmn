package co.edu.unal.gpn_camunda_unal.app.cancelacion.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.Task;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cancelaciones")
@RequiredArgsConstructor
@Slf4j
public class CancelacionController {

    private final RuntimeService runtimeService;

    private final TaskService taskService;

    @PostMapping("/iniciar")
    public ResponseEntity<String> iniciarCancelacion(@RequestParam String policyNumber,
                                                     @RequestParam String customerNumber,
                                                     @RequestParam boolean isPremium,
                                                     @RequestParam int numeroPeriodosAnteriores,
                                                     @RequestParam int nivelAsesor) {

        var processInstance = runtimeService.startProcessInstanceByKey("cancelacionProceso",
                Map.of("policyNumber", policyNumber,
                        "customerNumber", customerNumber,
                        "isPremium", isPremium,
                        "numeroPeriodosAnteriores", numeroPeriodosAnteriores,
                        "nivelAsesor", nivelAsesor));

        return ResponseEntity.ok("Proceso de cancelación iniciado para la póliza %s. La instancia es %s"
                .formatted(policyNumber, processInstance.getProcessInstanceId()));
    }

    @GetMapping("/clientes/tareas/{customerNumber}")
    public Set<String> getTasksForCustomer(@PathVariable String customerNumber) {
        List<Task> tasks = taskService.createTaskQuery()
                .taskAssignee(customerNumber)
                .list();
        return tasks.stream().map(t -> t.getId()).collect(Collectors.toSet());
    }

    @PostMapping("/clientes/aceptar-oferta-retencion/{taskId}")
    public ResponseEntity<String> aceptarOfertaRetencion(@PathVariable String taskId,
                                                         @RequestParam Boolean aceptada) {
        // Completar la tarea con los datos proporcionados
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task != null) {
            taskService.complete(taskId, Map.of("clienteAceptaOfertaRetencion", aceptada));
            return ResponseEntity.ok("Tarea completada exitosamente");
        } else {
            log.info("Tarea no encontrada...");
            return ResponseEntity.notFound().build();
        }
    }
}
