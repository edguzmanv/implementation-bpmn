package co.edu.unal.gpn_camunda_unal.app.reactivacion.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/reactivaciones")
@RequiredArgsConstructor
@Slf4j
public class ReactivacionController {

    private final RuntimeService runtimeService;

    @PostMapping("/iniciar")
    public ResponseEntity<String> iniciarReactivacion(@RequestParam String policyNumber,
                                                      @RequestParam String customerNumber,
                                                      @RequestParam int nivelAsesor) {

        var processInstance = runtimeService.startProcessInstanceByKey("reactivacionProceso",
                Map.of("policyNumber", policyNumber,
                        "customerNumber", customerNumber,
                        "nivelAsesor", nivelAsesor));

        return ResponseEntity.ok("Proceso de cancelación iniciado para la póliza %s. La instancia es %s"
                .formatted(policyNumber, processInstance.getProcessInstanceId()));
    }
}
