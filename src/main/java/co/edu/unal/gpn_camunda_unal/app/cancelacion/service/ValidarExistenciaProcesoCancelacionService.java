package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ValidarExistenciaProcesoCancelacionService implements JavaDelegate {

    private final RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        ProcessInstanceQuery query = runtimeService.createProcessInstanceQuery();

        query.variableValueEquals("policyNumber", policyNumber);

        List<ProcessInstance> processInstances = query.list();


        if (!processInstances.isEmpty()) {
            // la poliza ya esta siendo procesada en una instancia previa
            delegateExecution.setVariable("polizaYaTieneProcesoPrevio", true);
        } else {
            delegateExecution.setVariable("polizaYaTieneProcesoPrevio", false);
        }

        log.info("""
                                
                P1: Validar existencia de proceso actual para la póliza %s
                """.formatted(policyNumber));


    }
}
