package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class PostEvaluacionRetencion implements JavaDelegate {

    private final RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        Integer numeroPeriodosAnteriores = (Integer) delegateExecution
                .getVariable("numeroPeriodosAnteriores");

        Boolean isPremium = (Boolean) delegateExecution
                .getVariable("isPremium");

        var resultadoBMN = (List<HashMap>) delegateExecution.getVariable("resultado");

        var resultado = (HashMap<String, Object>) resultadoBMN.get(0);

        boolean retencionOfrecer = resultado.get("resultado").toString().contains("Aprobado");

        delegateExecution.setVariable("retencionAprobadoOfrecer", retencionOfrecer);

        log.info("""
                                
                P5: Para la póliza %s con entradas numeroPeriodosAnteriores %s
                Y isPremium %s
                La evaluación de posibilidad de retención ha sido %s
                """.formatted(policyNumber, numeroPeriodosAnteriores, isPremium, retencionOfrecer));


    }
}
