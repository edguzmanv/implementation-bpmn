package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class ConfirmacionCancelacionAprobadaService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        // logica que cancela la póliza (status active -> status cancelled)

        log.info("""
                                
                P13: Se confirma que la cancelación  
                ha sido aprobada para la póliza %s""".formatted(policyNumber));

    }
}
