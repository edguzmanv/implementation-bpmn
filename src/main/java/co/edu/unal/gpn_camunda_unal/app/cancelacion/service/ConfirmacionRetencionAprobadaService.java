package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class ConfirmacionRetencionAprobadaService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        log.info("""
                                
                P9: Se confirma que la oferta de retención luego de que el cliente   
                ha confirmado que la toma para la póliza %s""".formatted(policyNumber));

    }
}
