package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class OfertarRetencionClienteService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");
        var customerNumber = (String) delegateExecution.getVariable("customerNumber");

        log.info(""" 
                                
                P6: OfertarRetencionClienteService - Se ha aprobado retener al cliente
                para la póliza %s. Se notifica al cliente %s la oferta."""
                .formatted(policyNumber, customerNumber));

    }
}
