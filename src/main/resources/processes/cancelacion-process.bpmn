<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xmlns:camunda="http://camunda.org/schema/1.0/bpmn"
             xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/20100524/MODEL/BPMN20.xsd"
             targetNamespace="http://bpmn.io/schema/bpmn">
    <process id="cancelacionProceso" name="Proceso de cancelacion" isExecutable="true">
        <startEvent id="solicitudRecibida"/>
        <sequenceFlow sourceRef="solicitudRecibida" targetRef="validarExistenciaProcesoParaPoliza"/>
        <serviceTask id="validarExistenciaProcesoParaPoliza"
                     camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.ValidarExistenciaProcesoCancelacionService"/>

        <sequenceFlow sourceRef="validarExistenciaProcesoParaPoliza" targetRef="existeProcesoParaPolizaGateway" />

        <exclusiveGateway id="existeProcesoParaPolizaGateway"/>

        <sequenceFlow sourceRef="existeProcesoParaPolizaGateway" targetRef="validarEstadoPolizaService">
            <conditionExpression xsi:type="tFormalExpression">${!polizaYaTieneProcesoPrevio}</conditionExpression>
        </sequenceFlow>

        <sequenceFlow sourceRef="existeProcesoParaPolizaGateway" targetRef="notificaPolizaYaEnProcesoService">
            <conditionExpression xsi:type="tFormalExpression">${polizaYaTieneProcesoPrevio}</conditionExpression>
        </sequenceFlow>

        <serviceTask id="validarEstadoPolizaService" name="Validar estado de la poliza"
                     camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.ValidarEstadoCancelacionService"/>

        <sequenceFlow sourceRef="validarEstadoPolizaService" targetRef="polizaEsVigenteGateway" />

        <exclusiveGateway id="polizaEsVigenteGateway"/>

        <sequenceFlow sourceRef="polizaEsVigenteGateway" targetRef="notificaPolizaNoExisteService">
            <conditionExpression xsi:type="tFormalExpression">${!polizaExiste}</conditionExpression>
        </sequenceFlow>

        <sequenceFlow sourceRef="polizaEsVigenteGateway" targetRef="evaluarPosibilidadRetencionX">
            <conditionExpression xsi:type="tFormalExpression">${polizaExiste}</conditionExpression>
        </sequenceFlow>

        <businessRuleTask id="evaluarPosibilidadRetencionX" camunda:decisionRef="opcionDeRetencion"
        camunda:resultVariable="resultado" />

        <sequenceFlow sourceRef="evaluarPosibilidadRetencionX" targetRef="postEvaluacionRetention" />

        <serviceTask id="postEvaluacionRetention" name="Ejecucion post evaluacion" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.PostEvaluacionRetencion" />

        <sequenceFlow sourceRef="postEvaluacionRetention" targetRef="esPosibleRetenerGateway" />

        <exclusiveGateway id="esPosibleRetenerGateway" />

        <sequenceFlow sourceRef="esPosibleRetenerGateway" targetRef="ofertarRetencionClienteService" >
            <conditionExpression xsi:type="tFormalExpression" >${retencionAprobadoOfrecer}</conditionExpression>
        </sequenceFlow>

        <sequenceFlow sourceRef="esPosibleRetenerGateway" targetRef="noSeRetieneClienteUnionGateway" >
            <conditionExpression xsi:type="tFormalExpression">${!retencionAprobadoOfrecer}</conditionExpression>
        </sequenceFlow>

        <serviceTask id="ofertarRetencionClienteService" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.OfertarRetencionClienteService"/>

        <sequenceFlow sourceRef="ofertarRetencionClienteService" targetRef="ObtenerRespuestaRetencionCliente" />

        <userTask id="ObtenerRespuestaRetencionCliente" camunda:assignee="${customerNumber}" />

        <sequenceFlow sourceRef="ObtenerRespuestaRetencionCliente" targetRef="ofertaRetencionAceptadaGateway" />

        <exclusiveGateway id="ofertaRetencionAceptadaGateway"> </exclusiveGateway>

        <sequenceFlow sourceRef="ofertaRetencionAceptadaGateway" targetRef="procesarRecibosService"> 
                <conditionExpression xsi:type="tFormalExpression">${clienteAceptaOfertaRetencion}</conditionExpression>
        </sequenceFlow>

        <serviceTask id="confirmarRetencionTomada" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.ConfirmacionRetencionAprobadaService" />

        <sequenceFlow sourceRef="confirmarRetencionTomada" targetRef="procesarRecibosService" />

        <sequenceFlow sourceRef="ofertaRetencionAceptadaGateway" targetRef="noSeRetieneClienteUnionGateway">
            <conditionExpression xsi:type="tFormalExpression">${!clienteAceptaOfertaRetencion}</conditionExpression>
        </sequenceFlow>
        
        <serviceTask id="procesarRecibosService" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.ProcesarPagosYRecibosPorRetencionService"/>

        <sequenceFlow sourceRef="procesarRecibosService" targetRef="cancelacionAnuladaPorAceptacionRetencion" />
        
        <exclusiveGateway id="noSeRetieneClienteUnionGateway"> </exclusiveGateway>
        
        <sequenceFlow sourceRef="noSeRetieneClienteUnionGateway" targetRef="evaluarPosibilidadCancelacion" />

        <businessRuleTask id="evaluarPosibilidadCancelacion" camunda:decisionRef="opcionDeCancelacion"
                          camunda:resultVariable="resultadoCancelacion" />

        <sequenceFlow sourceRef="evaluarPosibilidadCancelacion" targetRef="PostEvaluacionCancelacion" />

        <serviceTask id="PostEvaluacionCancelacion" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.PostEvaluacionCancelacion"/>

        <sequenceFlow sourceRef="PostEvaluacionCancelacion" targetRef="cancelacionAprobadaGateway" />

        <exclusiveGateway id="cancelacionAprobadaGateway" />

        <sequenceFlow sourceRef="cancelacionAprobadaGateway" targetRef="cancelacionNotificarValidacionRechazada">
            <conditionExpression xsi:type="tFormalExpression">${!cancelacionAprobada}</conditionExpression>
        </sequenceFlow>

        <sequenceFlow sourceRef="cancelacionAprobadaGateway" targetRef="gestionarAutorizacionYFirmas">
            <conditionExpression xsi:type="tFormalExpression">${cancelacionAprobada}</conditionExpression>
        </sequenceFlow>

        <serviceTask id="cancelacionNotificarValidacionRechazada" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.NotificaCancelacionValidacionNoAprobadaService" />

        <sequenceFlow sourceRef="cancelacionNotificarValidacionRechazada" targetRef="cancelacionRechazadaEnValidacion" />

        <serviceTask id="gestionarAutorizacionYFirmas" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.GestionAutorizacionFirmasService" />
        
        <sequenceFlow sourceRef="gestionarAutorizacionYFirmas" targetRef="confirmarCancelacionAprobada" />

        <serviceTask id="confirmarCancelacionAprobada" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.ConfirmacionCancelacionAprobadaService" />

        <sequenceFlow sourceRef="confirmarCancelacionAprobada" targetRef="procesarPagosYRecibosCancelacionAprobada" />

        <serviceTask id="procesarPagosYRecibosCancelacionAprobada" camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.ProcesarPagosYRecibosPorCancelacionService" />

        <sequenceFlow sourceRef="procesarPagosYRecibosCancelacionAprobada" targetRef="cancelacionAprobada" />
        <!-- De finalización -->

        <serviceTask id="notificaPolizaNoExisteService" name="Servicio notifica poliza no existe"
                     camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.NotificaPolizaNoExisteService"/>

        <serviceTask id="notificaPolizaYaEnProcesoService" name="Servicio notifica poliza ya esta en proceso"
                     camunda:class="co.edu.unal.gpn_camunda_unal.app.cancelacion.service.NotificaPolizaYatieneProcesoService"/>

        <sequenceFlow sourceRef="notificaPolizaNoExisteService" targetRef="cancelacionRechazadaPolizaYaEnProceso"/>
        <endEvent id="cancelacionRechazada1"/>
        <endEvent id="cancelacionRechazadaEnValidacion" />
        <endEvent id="cancelacionRechazadaPolizaNoExiste"/>
        <endEvent id="cancelacionRechazadaPolizaYaEnProceso"/>
        <endEvent id="cancelacionAnuladaPorAceptacionRetencion"/>
        <endEvent id="cancelacionAprobada" />

    </process>

</definitions>
